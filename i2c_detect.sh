#! /bin/bash

echo  "A script to detect ic2-tools and print ouput to screen \n"
echo "list the enabled I2C bus:"
ls /dev/i2c*
echo  "list of installed buses: "
i2cdetect -l
echo "list of functionalities:"
i2cdetect -F 1
echo "Display the register address:"
i2cdetect -y -r 1
i2cdump -y 1 0x68

echo "use i2cget to read a byte at address 0x68:"
i2cget -y 1 0x68
