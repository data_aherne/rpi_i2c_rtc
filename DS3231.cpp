#include <iostream>
#include"DS3231.h"
#include<cstring>
#include<stdio.h>
#include<fcntl.h>
#include<sys/ioctl.h>
#include<unistd.h>
#include<linux/i2c-dev.h>
#include<string>
#define BUFFER_SIZE 19      //0x00 to 0x12
using namespace std;

int DS3231:: bcdToDec(char b) { return (b/16)*10 + (b%16); }


int DS3231:: openConnection(){
	string name;
	int file;
	name ="/dev/i2c-1"; 	 
	file=open(name.c_str(), O_RDWR);
   	ioctl(file, I2C_SLAVE,0x68); 
	return file;
}

int DS3231::writeRegister(unsigned int reg_add,unsigned char value){
	int file = openConnection();
	unsigned char buf[2];
	buf[0] = reg_add;
	buf[1] = value;
	write(file,buf,2);
	return 0;
}

void DS3231:: printTime(){ 
   int file;   
   file = openConnection();
   char buf[BUFFER_SIZE];
   read(file, buf, BUFFER_SIZE);
   printf("The RTC time is %02d:%02d:%02d\n", bcdToDec(buf[2]),
         bcdToDec(buf[1]), bcdToDec(buf[0]));
   close(file);
}


void DS3231:: printDate(){ 
   int file;   
   file = openConnection();
   char buf[BUFFER_SIZE];
   read(file, buf, BUFFER_SIZE);
   printf("The RTC Date is %02d/%02d/%02d\n", bcdToDec(buf[3]),
         bcdToDec(buf[5]), bcdToDec(buf[6]));
   close(file);
}


void DS3231:: printAlarm(){ 
   int file;   
   file = openConnection();
   char buf[BUFFER_SIZE];
   read(file, buf, BUFFER_SIZE);
   printf("The RTC alarm is set for %02d:%02d:%02d\n", bcdToDec(buf[9]),
         bcdToDec(buf[8]), bcdToDec(buf[7]));
   close(file);
}

void DS3231:: printTemp(){ 
   int file;   
   file = openConnection();
   char buf[BUFFER_SIZE];
   int tempin = buf[17];
   int tempdec = buf[18]>>6;
   int temp = (0.25*tempin)+(tempdec*0.25);
   printf("The RTC temp is %02d degrees\n ",temp);
   close(file);
}

void DS3231::changeHour(char hr){
	writeRegister(0x02,hr);
}
void DS3231::changeMin(char min){
	writeRegister(0x01,min);
}
void DS3231::setDate(char yy,char mm, char dd){
        writeRegister(0x04,dd);
        writeRegister(0x05,mm);
        writeRegister(0x06,yy);
}
void DS3231::setAlarmHour(char hr){
	writeRegister(0x09,hr);
}
void DS3231::setAlarmMin(char min){
	writeRegister(0x08,min);
}

void DS3231::setDate(char yy,char mm, char dd){
	writeRegister(0x04,dd);
	writeRegister(0x05,mm);
	writeRegister(0x06,yy);
}

void DS3231::resetClock(){
	changeHour(12);
	changeMin(0);
	setAlarmHour(12);
	setAlarmMin(0);
}

DS3231::~DS3231() {}
