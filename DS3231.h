#include<cstring>
#include<stdio.h>
#include<fcntl.h>
#include<sys/ioctl.h>
#include<unistd.h>
#include<linux/i2c-dev.h>
#include<string>
#define BUFFER_SIZE 19      //0x00 to 0x12
using namespace std;

class DS3231
{
	public:
		DS3231(){};
		int bcdToDec(char b);
		int openConnection();
		int writeRegister(unsigned int reg_address,unsigned char value);
		virtual void printTime();
		virtual void printDate(); 
		virtual void printAlarm(); 
		virtual void printTemp(); 
		void changeHour(char hr);
		void changeMin(char min);
		void setAlarmHour(char hr);
		void setAlarmMin(char min);
		void setDate(char yy,char mm, char dd);
		void resetClock();
		virtual ~DS3231();		

};
