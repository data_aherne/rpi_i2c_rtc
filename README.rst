#EE513: Assignment 1 2020/2021
#Interfacing Embedded Systems to the Real World:
#you are required to develop a self-contained embedded system that
#interfaces to the real world using electronic sensors. High-level
#software must be written to wrap the low-level interface.
#This assignment is worth 10% of the overall module mark for EE513.
#author: Derek Aherne
#student number:
#email: derek.aherne6@mail.dcu.ie
#date: 02/03/2021
#Hardware:
## Raspberry Pi 4 Model B
## HDMI monitor
## USB Keyboard and mouse
## Micro USB charger
## DS3231 RTC
## ADXL345
